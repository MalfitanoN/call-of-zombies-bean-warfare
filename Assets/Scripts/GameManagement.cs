﻿using UnityEngine;
using System.Collections;

public class GameManagement : MonoBehaviour {
    public int round = 1;
    int zombiesInRound = 10;
    int zombiesSpawnedInRound = 0;
    float zombieSpawnTimer = 0;
    public Transform[] zombieSpawnPoints;
    public GameObject zombieEnemy;

    static int playerScore = 0;
    static int playerCash = 0;

    public GUISkin mySkin;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        if (zombiesSpawnedInRound < zombiesInRound) {
            if (zombieSpawnTimer > 30)
            {
                spawnZombie();
                zombieSpawnTimer = 0;
            }
            else {
                zombieSpawnTimer++;
            }
        }
	}

    public static void AddPoints(int pointValue) {
        playerScore += pointValue;
        playerCash += pointValue;
    }

    void spawnZombie() {
            Vector3 randomSpawnPoint = zombieSpawnPoints[Random.Range (0, zombieSpawnPoints.Length )].position;
            Instantiate(zombieEnemy, randomSpawnPoint, Quaternion.identity);
            zombiesSpawnedInRound++;
        }
    void OnGUI()
    {
        GUI.skin = mySkin;
        GUIStyle style1 = mySkin.customStyles[0];

        GUI.Label(new Rect(40, Screen.height - 50, 100, 60), "Score:");
        GUI.Label(new Rect(100, Screen.height - 50, 160, 60), "" + playerScore, style1);

        GUI.Label(new Rect(40, Screen.height - 80, 100, 60), "$: ");
        GUI.Label(new Rect(100, Screen.height - 80, 160, 60), "" + playerCash, style1);
    }
}
