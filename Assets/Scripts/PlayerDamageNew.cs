using UnityEngine;
using System.Collections;

#pragma warning disable 0219
#pragma warning disable 0414

[ExecuteInEditMode]
public class PlayerDamageNew : MonoBehaviour
{
    /**
    *  Script written by OMA [www.armedunity.com]
    **/


    public float hitPoints;
    public int regenerationSkill;
    public int shieldSkill;
    public AudioClip painSound;
    public AudioClip die;
    public Transform deadReplacement;
    public GUISkin mySkin;
    public GameObject explShake;
    private GameObject radar;
    public float maxHitPoints;
    public Texture damageTexture;
    private float time = 0.0f;
    private float alpha;
    private bool callFunction = false;
    private ScoreManager scoreManager;

    void Start()
    {
        maxHitPoints = hitPoints;
        alpha = 0;
    }

    void Update()
    {
        if (time > 0)
        {
            time -= Time.deltaTime;
        }
        alpha = time;
        if (hitPoints <= maxHitPoints)
        {
            hitPoints += Time.deltaTime * regenerationSkill;
        }

        if (hitPoints > maxHitPoints)
        {
            float convertToScore = hitPoints - maxHitPoints;
            scoreManager = GameObject.Find("ScoreManager").GetComponent<ScoreManager>();
            //scoreManager.addScore(convertToScore);
            hitPoints = maxHitPoints;
        }
    }

    void PlayerDamage(int damage)
    {
        if (hitPoints < 0.0f)
            return;

        damage -= shieldSkill;

        if (damage > 0)
        {
            // Apply damage
            hitPoints -= damage;
            GetComponent<AudioSource>().PlayOneShot(painSound, 1.0f / GetComponent<AudioSource>().volume);
            time = 2.0f;


            // Are we dead?
            if (hitPoints <= 0.0f)
                StartCoroutine(Die());
        }
        else
        {
            damage = 0;
        }
    }

    //Picking up MedicKit
    void Medic(int medic)
    {

        hitPoints += medic;

        if (hitPoints > maxHitPoints)
            hitPoints = maxHitPoints;
    }

    IEnumerator Die()
    {
        if (callFunction)
            yield break;
        callFunction = true;

        if (die && deadReplacement)
            AudioSource.PlayClipAtPoint(die, transform.position);

        // Disable all script behaviours (Essentially deactivating player control)
        Component[] coms = GetComponentsInChildren<MonoBehaviour>();
        foreach (var b in coms)
        {
            MonoBehaviour p = b as MonoBehaviour;
            if (p)
                p.enabled = false;
        }
        // Disable all renderers
        Renderer[] gos = GetComponentsInChildren<Renderer>();
        foreach (Renderer go in gos)
        {
            go.enabled = false;

        }
        if (radar != null)
        {
            radar = GameObject.FindWithTag("Radar");
            radar.gameObject.SetActive(false);
        }
        Instantiate(deadReplacement, transform.position, transform.rotation);
        yield return new WaitForSeconds(4.5f);
        //Destroy (transform.root.gameObject);
    }
    void OnGUI()
    {
        GUI.skin = mySkin;
        GUIStyle style1 = mySkin.customStyles[0];
        GUI.Label(new Rect(40, Screen.height - 50, 60, 60), " Health: ");
        GUI.Label(new Rect(100, Screen.height - 50, 60, 60), "" + hitPoints.ToString("F0"), style1);

        GUI.color = new Color(1.0f, 1.0f, 1.0f, alpha); //Color (r,g,b,a)
        GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), damageTexture);
    }

    void Exploasion()
    {
        explShake.GetComponent<Animation>().Play("exploasion");
    }
}