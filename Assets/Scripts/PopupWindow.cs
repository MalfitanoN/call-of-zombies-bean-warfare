﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PopupWindow : MonoBehaviour {

    public GameObject window;
    public Text messageField;

	// Use this for initialization
	public void Show(string message) {
        messageField.text = message;
        window.SetActive(true);
	}
	
	// Update is called once per frame
	public void Hide () {
        window.SetActive(false);
	}
}
