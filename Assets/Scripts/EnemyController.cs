﻿using UnityEngine;
using System.Collections;

public class EnemyController : MonoBehaviour {
    NavMeshAgent nav;
    Transform player;
    float health = 20;
    GameManagement game;
    CapsuleCollider capsuleCollider;

    // Use this for initialization
    void Awake() {
        nav = GetComponent<NavMeshAgent>();
        player = GameObject.FindGameObjectWithTag("Player").transform;
        game = FindObjectOfType<GameManagement>();
        health = 20 + (1.25F * game.round);
        capsuleCollider = GetComponent<CapsuleCollider>();
    }

    // Update is called once per frame
    void Update() {
        nav.SetDestination(player.position);
    }

    void ApplyDamage(float damage) {
        health -= damage;
        if (health <= 0) {
            Death ();
        }
    }

    void Death() {
        nav.Stop();
        capsuleCollider.isTrigger = true;
        Destroy(gameObject);
    }
}
